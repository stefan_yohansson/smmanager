<?php

use SMManager\Role,
SMManager\User,
SMManager\Tests\Fixtures\Fixtures,
SMManager\Tests\Fixtures\FixturesData;

class UserTest extends TestCase {

  public function setUp()
  {
    parent::setUp();
    Fixtures::roles();
  }

  /**
  * A basic functional test example.
  *
  * @return void
  */
  public function test_create_users()
  {
    $input = FixturesData::user();
    $input = array_merge($input, array('roles_id' => Role::ADMIN));

    $user = User::create($input);

    $this->assertTrue($user->id > 0);
  }

}
