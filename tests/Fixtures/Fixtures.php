<?php namespace SMManager\Tests\Fixtures;

use SMManager\Role;

class Fixtures {

  public static function roles() {
    $roles = FixturesData::roles();
    foreach ($roles as $r) {
      $role = Role::create(array('name' => $r));
      $role->save();
    }
  }

}
