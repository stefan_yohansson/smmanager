<?php namespace SMManager\Tests\Fixtures;

class FixturesData {

  public static $roles_data = ['Admin', 'Social Media'];

  public static function roles()
  {
    return self::$roles_data;
  }

  public static function user()
  {
    return array(
      'name' => 'Admin',
      'email' => 'admin@teste.com',
      'password' => \Hash::make('admin'),
    );
  }
}
