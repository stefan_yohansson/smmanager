<?php

class RoleTest extends TestCase {

	public function test_create_role()
	{
        $role = SMManager\Role::create(array(
            'name' => 'Admin'
        ));

		$this->assertTrue($role->id > 0);
	}

}
