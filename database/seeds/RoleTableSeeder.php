<?php

use Illuminate\Database\Seeder;
class RoleTableSeeder extends Seeder {

    public function run()
    {
        DB::table('roles')->delete();

        SMManager\Role::create(array('id' => 1, 'name' => 'Admin'));
        SMManager\Role::create(array('id' => 2, 'name' => 'Social Media'));
    }

}
