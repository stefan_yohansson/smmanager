<?php

use Illuminate\Database\Seeder;
class UserTableSeeder extends Seeder {

    public function run()
    {
        DB::table('users')->delete();

        SMManager\User::create(array('email' => 'foo@bar.com', 'password' => Hash::make('bar'), 'name' => 'Nome Bom Foo Bario', 'roles_id' => 1));
    }

}
