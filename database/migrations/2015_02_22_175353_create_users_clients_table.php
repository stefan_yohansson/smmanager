<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersClientsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
    Schema::create('users_clients', function($table) {
      $table->increments('id');
			$table->integer('clients_id');
      $table->integer('users_id');

      /* fks */
      $table->foreign('clients_id')
          ->references('id')->on('clients')->onDelete('cascade');
      $table->foreign('users_id')
      ->references('id')->on('users')->onDelete('cascade');

    });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
    Schema::drop('users_clients');
	}

}
