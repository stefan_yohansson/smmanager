<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIterationTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
    Schema::create('iterations', function($table) {
      $table->increments('id');
			$table->integer('clients_id');
      $table->integer('users_id');
      $table->dateTime('iteration_date');
      $table->boolean('iteration')->default(false);
      $table->boolean('post')->default(false);

      /* fks */
      $table->foreign('clients_id')
      ->references('id')->on('clients')->onDelete('cascade');
      $table->foreign('users_id')
      ->references('id')->on('users')->onDelete('cascade');

    });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
    Schema::drop('iterations');
	}

}
