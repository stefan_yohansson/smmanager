<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSocialpagesAccounts extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('socialpages_accounts', function($table) {
      $table->increments('id');
      $table->string('user');
      $table->string('password');
			$table->boolean('active')->default(true);
			$table->integer('social_network_id');
      $table->timestamps();

      $table->foreign('social_network_id')
    	->references('id')->on('clients_socialpages')->onDelete('cascade');

    });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('socialpages_accounts');
	}

}
