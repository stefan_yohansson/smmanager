<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterIterationAddSocialnetworkColumn extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
	  Schema::table('iterations', function($table) {
	    $table->integer('clients_socialpages_id');

			$table->foreign('clients_socialpages_id')
      ->references('id')->on('clients_socialpages')->onDelete('cascade');

	  });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
    Schema::table('iterations', function($table) {
      $table->dropColumn('clients_socialpages_id');
    });
	}

}
