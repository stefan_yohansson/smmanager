<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClientsSocialpagesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
    Schema::create('clients_socialpages', function($table) {
      $table->increments('id');
			$table->string('social_network');
      $table->string('address');
      $table->integer('target_posts');
      $table->string('target_posts_period');
      $table->integer('target_iterations');
      $table->string('target_iterations_period');
      $table->integer('clients_id');
      $table->timestamps();

      $table->foreign('clients_id')
    	->references('id')->on('clients')->onDelete('cascade');

    });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
    Schema::drop('clients_socialpages');
	}

}
