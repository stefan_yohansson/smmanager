<?php namespace SMManager\Commands;

use SMManager\Commands\Command;
use SMManager\Client;
use Illuminate\Contracts\Bus\SelfHandling;

class SyncFacebook extends Command implements SelfHandling {
    
    /*
     * $post is a boolean that allow sync to get posts in page
     */
    protected $post;
    
    /*
     * $iteration is a boolean that allow sync to get iterations in page
     */
    protected $iteration;
    
    /*
     * $date_start date start for range search of posts and iterations
     */
    protected $date_start;
    
    /*
     * $date_end date end for range search of posts and iterations
     */
    protected $date_end;

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct($post = true, $iteration = true, $date_start = null, $date_end = null)
	{
		$this->post = $post;
        $this->iteration = $iteration;
        
        if(!$date_start)
        {
            $date_start = date('Y-m-d', strtotime('-1 days'));
        }
        
        $this->date_start = $date_start;
        
        if(!$date_end)
        {
            $date_end = date('Y-m-d');
        }
        $this->date_end = $date_end;
	}

	/**
	 * Execute the command.
	 *
	 * @return void
	 */
	public function handle()
	{
        $fb = \App::make('SammyK\LaravelFacebookSdk\LaravelFacebookSdk');
        
        $token = $this->getToken();
        
        $fb->setDefaultAccessToken($token);
        
        $clients = Client::all();
        
        foreach($clients as $client)
        {
            $networks = $client->socialpages()->where('social_network', 'facebook')->get();
            
            if(!$networks)
            {
                continue;
            }
            
            foreach($networks as $network)
            {
                try {
                    $response = $fb->get("/{$network->address}/posts?since={$this->date_start}&until={$this->date_end}");
                    $graphList = $response->getGraphList();
                    
                    foreach ($graphList as $graphObject) {
                        $obj_arr = $graphObject->asArray();
                        var_dump($obj_arr);die;
                    }
                    return [true, $response];
                } catch (Facebook\Exceptions\FacebookSDKException $e) {
                    return [false, $e->getMessage()];
                }
            }
            
        }
        
        
		return [false, 'algum erro ocorreu.'];
	}
    
    protected function getToken()
    {
        $ch = curl_init("https://graph.facebook.com/oauth/access_token?client_id=861577133904059&client_secret=1a346a7960307509e5a119720209748c&grant_type=client_credentials");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_NOSIGNAL, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT_MS, 30000);
        $data = curl_exec($ch);
        $curl_errno = curl_errno($ch);
        $curl_error = curl_error($ch);
        curl_close($ch);
        
        if ($curl_errno > 0) 
        {
            var_dump("cURL Error [getToken] ($curl_errno): $curl_error\n");die;
        } 
        else 
        {
            $token = str_replace('access_token=', '', $data);
        }
        
        return $token;
    }

}
