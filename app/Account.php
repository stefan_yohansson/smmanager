<?php namespace SMManager;

use Illuminate\Database\Eloquent\Model;

class Account extends Model {

	protected $table = 'socialpages_accounts';

	protected $fillable = ['user', 'password', 'social_network_id'];

	protected $guarded = ['created_at', 'updated_at', 'id'];

	public function socialpages() {
		return $this->belongsTo('SMManager\\SocialPage', 'social_network_id', 'id');
	}

}
