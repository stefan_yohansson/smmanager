<?php namespace SMManager\Console\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use SMManager\Commands\SyncFacebook;

class SyncNetworks extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'network:sync';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Sync the posts and iterations in all or separate networks.';
	
	protected $providers = array(
								 'facebook' => 'providerFacebook',
								 'twitter' => 'providerTwitter',
								 'instagram' => 'providerInstagram',
								 'google_plus' => 'providerGooglePlus',
								 );

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct(\Illuminate\Contracts\Bus\Dispatcher $bus)
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
		if(in_array($this->option('network'), array_keys($this->providers)))
		{
			$this->info("processing: {$this->option('network')}");
			$f_call = call_user_func_array(array($this, $this->providers[$this->option('network')]), array());
			list($status, $message) = $f_call;
			
			if(!$status)
			{
				$this->error($message);
				
				return;
			}
			
			$this->info($message);
		}
		
		$this->error('You must choose an valid option! ('.implode(', ', array_keys($this->providers)).')');
		//
	}
	
	protected function providerFacebook()
	{
		$return = \Bus::dispatch(
			new SyncFacebook()
		);
		
		return $return;
	}
	
	protected function providerTwitter()
	{
		
	}
	
	protected function providerInstagram()
	{
		
	}
	
	protected function providerGooglePlus()
	{
		
	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return [
			['post', InputArgument::OPTIONAL, 'process post?'],
			['iteration', InputArgument::OPTIONAL, 'process iteration?'],
			['date_start', InputArgument::OPTIONAL, 'date start for crawling'],
			['date_end', InputArgument::OPTIONAL, 'date end for crawling'],
		];
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return [
			['network', null, InputOption::VALUE_OPTIONAL, 'network to process.', null],
		];
	}

}
