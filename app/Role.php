<?php namespace SMManager;

use Illuminate\Database\Eloquent\Model;

class Role extends Model {
    
    const ADMIN        = 1;
    const SOCIAL_MEDIA = 2;

    protected $table = 'roles';

    protected $guarded = ['id'];
    
    protected $fillable = ['name'];

    public $timestamps = false;

    public function users() {
        return $this->belongsTo('User', 'roles_id', 'id');
    }
}
