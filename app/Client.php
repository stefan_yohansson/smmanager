<?php namespace SMManager;

use Illuminate\Database\Eloquent\Model;

class Client extends Model {

  protected $table = 'clients';

  protected $fillable = ['name', 'phone', 'email', 'active'];

  protected $guarded = ['created_at', 'updated_at', 'id'];

  protected static $labels = [
    'id'   => '#',
    'name' => 'Name',
    'phone' => 'Phone Contact',
    'email' => 'Email',
    'created_at' => 'Created At',
    'updated_at' => 'Updated At',
    'active' => 'Active',
  ];

  public function iterations()
  {
    return $this->hasMany('SMManager\\Iteration', 'clients_id', 'id');
  }

  public function users()
  {
    return $this->belongsToMany('SMManager\\User', 'users_clients', 'clients_id', 'users_id');
  }

  public function socialpages()
  {
    return $this->hasMany('SMManager\\SocialPage', 'clients_id', 'id');
  }

  public static function format($instance)
  {
    $data = $instance->toArray();

    foreach($data as $key => $value)
    {
      if(in_array($key, ['created_at', 'updated_at']))
      {
        $data[$key] = self::formatDate($value);
      }

      if(in_array($key, ['active']))
      {
        $data[$key] = self::formatBoolean($value);
      }

      $data[self::formatLabel($key)] = $data[$key];
      unset($data[$key]);
    }

    return $data;
  }

  protected static function formatLabel($key)
  {
    if(!in_array($key, array_keys(self::$labels)))
    {
      return $key;
    }

    return self::$labels[$key];
  }

  public static function formatDate($value)
  {
    return date('d/m/Y H:i:s', strtotime($value));
  }

  public static function formatBoolean($value)
  {
    $label = array('danger', 'success');

    $text = 'No';
    if($value)
    {
      $text = 'Yes';
    }

    return '<label class="label label-'.$label[$value].'"> '.$text.' </label>';
  }

}
