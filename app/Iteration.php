<?php namespace SMManager;

use Illuminate\Database\Eloquent\Model;

class Iteration extends Model {

  protected $table = 'iterations';

  public $timestamps = false;

  protected $fillable = ['clients_id', 'clients_socialpages_id', 'users_id', 'iteration_date', 'iteration', 'post'];

  protected $guarded = ['id'];

  public function users() {
    return $this->belongsTo('User', 'users_id', 'id');
  }

  public function clients() {
    return $this->belongsTo('Client', 'clients_id', 'id');
  }

}
