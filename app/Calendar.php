<?php namespace SMManager;

class Calendar
{
  public static function getMonthDays($month, $year)
  {
    return cal_days_in_month(CAL_GREGORIAN, $month, $year);
  }

  public static function getMonths()
  {
    return ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'];
  }

  public static function isDayToday($day)
  {
    return date('d/m/Y', strtotime('now')) == date('d/m/Y',strtotime($day));
  }

  public static function isDayWeekend($day)
  {
    return date('d/m/Y', strtotime('sunday', strtotime($day))) == date('d/m/Y', strtotime($day)) || date('d/m/Y', strtotime('saturday', strtotime($day))) == date('d/m/Y', strtotime($day));
  } 
}
