<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'HomeController@index');

Route::get('home', 'HomeController@index');

Route::group(array('middleware' => 'auth', 'prefix' => 'admin'), function() {
    Route::resource('users', 'UserController');
    Route::resource('roles', 'RoleController');
    Route::resource('clients', 'ClientController');

    Route::get('accounts/{id?}', 'NetworkController@accounts');

    Route::get('iterations/increment/{id}', 'ClientController@incrementIteration');
    Route::get('iterations/decrement/{id}', 'ClientController@decrementIteration');

    Route::get('users/attach/clients/{id}', 'UserController@attachClient');
    Route::post('users/attach/clients/{id}', 'UserController@postAttachClient');

    Route::get('clients/attach/socialpages/{id}', 'ClientController@attachSocialPages');
    Route::post('clients/attach/socialpages/{id}', 'ClientController@postAttachSocialPages');
	Route::get('clients/{client_id}/edit/socialpages/{id}', 'ClientController@editSocialPages');
    Route::post('clients/{client_id}/edit/socialpages/{id}', 'ClientController@postEditSocialPages');
    Route::delete('clients/detach/socialpages/{id}/{clients_id}', 'ClientController@postDetachSocialPages');

    Route::get('clients/{clients_id}/socialpages/{socialpages_id}/attach/accounts', 'ClientController@attachSocialPagesAccounts');
    Route::post('clients/{clients_id}/socialpages/{socialpages_id}/attach/accounts', 'ClientController@postAttachSocialPagesAccounts');
    Route::delete('clients/{clients_id}/socialpages/{socialpages_id}/detach/accounts/{id}', 'ClientController@postDetachSocialPagesAccounts');
});

Route::controllers([
	'auth' => 'Auth\AuthController',
	'password' => 'Auth\PasswordController',
]);
