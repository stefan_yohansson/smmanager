<?php namespace SMManager\Http\Controllers;

use SMManager\Http\Requests;
use SMManager\Http\Controllers\Controller;

use Illuminate\Http\Request;
use SMManager\Role;

class RoleController extends Controller {

	/**
	* Display a listing of the role.
	*
	* @return Response
	*/
	public function index()
	{
		$roles = Role::paginate(15);

		return view('roles', compact('roles'));
	}

	/**
	* Show the form for creating a new role.
	*
	* @return Response
	*/
	public function create()
	{
		return view('role_create');
	}

	/**
	* Store a newly created role in storage.
	*
	* @return Response
	*/
	public function store(Request $request)
	{
		$input = \Input::all();

		$validator = \Validator::make($input, array(
			'name' => 'required|min:2',
		));

		if ($validator->fails())
		{
			$this->throwValidationException(
			$request, $validator
		);
	}

	$role = Role::create($input);

	return redirect('admin/roles/' . $role->id);

}

/**
* Display the specified role.
*
* @param  int  $id
* @return Response
*/
public function show($id)
{
	$role = Role::find($id);

	if(!$role)
	{
		return redirect('admin/roles');
	}

	$role = $role->toArray();

	return view('role', compact('role', 'id'));

}

/**
* Show the form for editing the specified role.
*
* @param  int  $id
* @return Response
*/
public function edit($id)
{
	$role = Role::find($id);

	if(!$role)
	{
		return redirect('admin/roles')->withErrors(['id' => 'id does not exists.']);
	}

	return view('role_update', compact('role', 'id'));
}

/**
* Update the specified role in storage.
*
* @param  int  $id
* @return Response
*/
public function update(Request $request, $id)
{
	$input = \Input::all();
	$role = Role::find($id);

	$validator = \Validator::make($input, array(
		'name' => 'required|min:2',
	));

	if ($validator->fails())
	{
		$this->throwValidationException(
		$request, $validator
	);
}

$role->name = $input['name'];
$role->save();

return redirect('admin/roles/' . $role->id);

}

/**
* Remove the specified role from storage.
*
* @param  int  $id
* @return Response
*/
public function destroy($id)
{
	$role = Role::find($id);

	if(!$role)
	{
		return redirect('admin/roles')->withErrors(['id' => 'id does not exists.']);
	}

	$role->delete();

	return redirect('admin/roles');

}

}
