<?php namespace SMManager\Http\Controllers;

use SMManager\Http\Requests;
use SMManager\Http\Controllers\Controller;

use Illuminate\Http\Request;

use SMManager\Account;
use SMManager\Client;
use SMManager\User;
use SMManager\Role;

class NetworkController extends Controller {

	public function accounts($id = null)
	{
		$param_id = $id;
		
		if(is_null($param_id))
		{
			$id = \Auth::user()->id;	
		}
	
		$user = User::find($id);
		$clients = $user->clients;

		if($user->roles_id == Role::ADMIN && !$param_id)
		{
			$clients = Client::all();
		}
		
		$users = User::all();

		return view('accounts', compact('clients', 'users'));
	}

}
