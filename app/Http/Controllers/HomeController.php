<?php namespace SMManager\Http\Controllers;

use SMManager\Calendar;
use SMManager\User;
use SMManager\SocialPage;
use SMManager\Role;
use SMManager\Client;

class HomeController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Home Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders your application's "dashboard" for users that
	| are authenticated. Of course, you are free to change or remove the
	| controller as you wish. It is just here to get your app started!
	|
	*/

	/**
	* Create a new controller instance.
	*
	* @return void
	*/
	public function __construct()
	{
		$this->middleware('auth');
	}

	/**
	* Show the application dashboard to the user.
	*
	* @return Response
	*/
	public function index()
	{
		$month = date('m');
		$year = date('Y');

		if(\Input::has('month'))
		{
			$month = \Input::get('month');
		}

		if(\Input::has('year'))
		{
			$year = \Input::get('year');
		}

		if(\Input::has('clients'))
		{
			$clients_arr_get = \Input::get('clients');
		}

		$month_days = Calendar::getMonthDays($month, $year);
		$user = User::find(\Auth::user()->id);

		if(isset($clients_arr_get))
		{
			$clients = $user->clients()->whereIn('clients_id', $clients_arr_get)->get();
			if($user->roles_id == Role::ADMIN)
			{
				$clients = Client::whereIn('id', $clients_arr_get)->get();
			}
		} else {
			$clients = $user->clients;
			if($user->roles_id == Role::ADMIN)
			{
				$clients = Client::all();
			}
		}

		$dates_target = SocialPage::checkTargetInMonth($clients, compact('month', 'year', 'month_days'));

		$clients_arr = $clients->toArray();
		$clients_id = [];
		foreach($clients_arr as $client)
		{
			$clients_id[] = $client['id'];
		}

		$months_arr = Calendar::getMonths();

		return view('home', compact('month_days', 'dates_target', 'clients', 'month', 'year', 'months_arr', 'clients_id'));
	}

}
