<?php namespace SMManager\Http\Controllers;

use SMManager\Http\Requests;
use SMManager\Http\Controllers\Controller;

use Illuminate\Http\Request;

use SMManager\User;
use SMManager\Client;

class UserController extends Controller {

  /**
  * Display a listing of the user.
  *
  * @return Response
  */
  public function index()
  {
    $users = User::paginate(15);

    return view('users', compact('users'));
  }

  /**
  * Show the form for creating a new user.
  *
  * @return Response
  */
  public function create()
  {
    return view('user_create');
  }

  /**
  * Store a newly created user in storage.
  *
  * @return Response
  */
  public function store(Request $request)
  {
    $input = \Input::all();

    $validator = \Validator::make($input, array(
      'name' => 'required|min:4',
      'email'     => 'required|email|unique:users',
      'password'  => 'required|min:8|same:password_confirmation'
    ));

    if ($validator->fails())
    {
      $this->throwValidationException(
        $request, $validator
      );
    }

    $input['password'] = \Hash::make($input['password']);
    $user = User::create($input);

    return redirect('admin/users/' . $user->id);
  }

  /**
  * Display the specified user.
  *
  * @param  int  $id
  * @return Response
  */
  public function show($id)
  {
    $user = User::find($id);

    if(!$user)
    {
      return redirect('admin/users');
    }

    $clients = $user->clients;
    $user = User::format($user);

    return view('user', compact('user', 'clients', 'id'));
  }

  /**
  * Show the form for editing the specified user.
  *
  * @param  int  $id
  * @return Response
  */
  public function edit($id)
  {
    $user = User::find($id);

    if(!$user)
    {
      return redirect('admin/users')->withErrors(['id' => 'id does not found.']);
    }

    return view('user_update', compact('user', 'id'));
  }

  /**
  * Update the specified user in storage.
  *
  * @param  int  $id
  * @return Response
  */
  public function update(Request $request, $id)
  {
    $input = \Input::all();
    $user = User::find($id);

    $validator = \Validator::make($input, array(
      'name'      => 'required|min:4',
      'email'     => 'required|email',
      'roles_id'     => 'required',
    ));

    if ($validator->fails())
    {
      $this->throwValidationException(
      $request, $validator
    );
  }

  $user->name = $input['name'];
  $user->email = $input['email'];
  $user->roles_id = $input['roles_id'];

  $user->save();

  return redirect('admin/users/' . $user->id);
  }

  /**
  * Remove the specified user from storage.
  *
  * @param  int  $id
  * @return Response
  */
  public function destroy($id)
  {
    $user = User::find($id);

    if(!$user)
    {
      return redirect('admin/users')->withErrors(['id' => 'id does not exists.']);
    }

    $user->delete();

    return redirect('admin/users');
  }

  public function attachClient($id)
  {
    $clients = Client::all();
    $user = User::find($id);

    $attached_clients = [];

    foreach($user->clients as $client)
    {
      $attached_clients[] = $client->id;
    }

    return view('users_attach_clients', compact('clients', 'attached_clients', 'id'));
  }

  public function postAttachClient(Request $request, $id)
  {
    $clients = \Input::get('clients');
    $user = User::find($id);

    if(!($clients && $user))
    {
      return redirect('admin/users');
    }

    $user->clients()->sync($clients);

    return redirect('admin/users/' . $id);
  }

}
