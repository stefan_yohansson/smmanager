<?php namespace SMManager\Http\Controllers;

use SMManager\Http\Requests;
use SMManager\Http\Controllers\Controller;

use Illuminate\Http\Request;
use SMManager\Client;
use SMManager\Iteration;
use SMManager\SocialPage;
use SMManager\Account;

class ClientController extends Controller {

	/**
	* Display a listing of the client.
	*
	* @return Response
	*/
	public function index()
	{
		$clients = Client::paginate(15);

		return view('clients', compact('clients'));
	}

	/**
	* Show the form for creating a new client.
	*
	* @return Response
	*/
	public function create()
	{
		return view('client_create');
	}

	/**
	* Store a newly created client in storage.
	*
	* @return Response
	*/
	public function store(Request $request)
	{
		$input = \Input::all();

		if(!in_array('active', array_keys($input)))
		{
			$input['active'] = false;
		}

		$validator = \Validator::make($input, array(
			'name' => 'required|min:4',
			'email'     => 'required|email',
			'active' => 'required'
		));

		if ($validator->fails())
		{
			$this->throwValidationException(
				$request, $validator
			);
		}

		$client = Client::create($input);

		return redirect('admin/clients/' . $client->id);

	}

	/**
	* Display the specified client.
	*
	* @param  int  $id
	* @return Response
	*/
	public function show($id)
	{
		$client = Client::find($id);

		if(!$client)
		{
			return redirect('admin/clients');
		}

		$users = $client->users;
		$networks = $client->socialpages;
		$client = Client::format($client);

		return view('client', compact('client', 'users', 'networks', 'id'));
	}

	/**
	* Show the form for editing the specified client.
	*
	* @param  int  $id
	* @return Response
	*/
	public function edit($id)
	{
		$client = Client::find($id);

		if(!$client)
		{
			return redirect('admin/clients')->withErrors(['id' => 'id does not found.']);
		}

		return view('client_update', compact('client', 'id'));
	}

	/**
	* Update the specified client in storage.
	*
	* @param  int  $id
	* @return Response
	*/
	public function update(Request $request, $id)
	{
		$input = \Input::all();
		$client = Client::find($id);

		if(!in_array('active', array_keys($input)))
		{
			$input['active'] = false;
		}

		$validator = \Validator::make($input, array(
			'name' => 'required|min:4',
			'email'     => 'required|email',
			'active' => 'required'
		));

		if ($validator->fails())
		{
			$this->throwValidationException(
			$request, $validator
		);
	}

	$client->name = $input['name'];
	$client->email = $input['email'];
	$client->phone = $input['phone'];
	$client->active = $input['active'];

	$client->save();

	return redirect('admin/clients/' . $client->id);

	}

	/**
	* Remove the specified client from storage.
	*
	* @param  int  $id
	* @return Response
	*/
	public function destroy($id)
	{
		$client = Client::find($id);

		if(!$client)
		{
			return redirect('admin/clients')->withErrors(['id' => 'id does not exists.']);
		}

		$client->delete();

		return redirect('admin/clients');

	}

	public function attachSocialPages($id)
	{
		return view('clients_attach_socialpages', compact('id'));
	}

	public function postAttachSocialPages(Request $request, $id)
	{
		$input = \Input::all();

		$validator = \Validator::make($input, array(
			'social_network' => 'required',
			'address'     => 'required',
			'target_posts'  => 'required|integer',
			'target_iterations'  => 'required|integer'
		));

		if ($validator->fails())
		{
			$this->throwValidationException(
				$request, $validator
			);
		}

		$social_page = SocialPage::create($input);
	
		return redirect('admin/clients/' . $id);

	}
	
	public function editSocialPages($id, $client_id)
	{
		$social_page = SocialPage::find($id);
		
		return view('clients_edit_socialpages', compact('social_page', 'id', 'client_id'));
	}
	
	public function postEditSocialPages(Request $request, $id, $client_id)
	{
		$input = \Input::all();

		$validator = \Validator::make($input, array(
			'social_network' => 'required',
			'address'     => 'required',
			'target_posts'  => 'required|integer',
			'target_iterations'  => 'required|integer'
		));

		if ($validator->fails())
		{
			$this->throwValidationException(
				$request, $validator
			);
		}

		$social_page = SocialPage::find($id);
		
		$social_page->social_network = \Input::get('social_network');
		$social_page->address = \Input::get('address');
		$social_page->target_posts = \Input::get('target_posts');
		$social_page->target_iterations = \Input::get('target_iterations');
		
		$social_page->save();
	
		return redirect('admin/clients/' . $client_id);
	}

	public function postDetachSocialPages(Request $request, $id, $clients_id)
	{
		$social_page = SocialPage::find($id);

		if(!$social_page)
		{
			return redirect('admin/clients')->withErrors(['id' => 'id does not exists.']);
		}

		$client->delete();

		return redirect('admin/clients/' . $clients_id);
	}

	public function incrementIteration($id)
	{
		$type = \Input::get('type');
		$day = \Input::get('day');
		$month = \Input::get('month');
		$year = \Input::get('year');

		$socialpage = SocialPage::find($id);
		$date = date('Y-m-d H:i:s', strtotime($year.'-'.$month.'-'.$day));

		$iteration = Iteration::create(array(
			'type' => $type,
			'iteration_date' => $date,
			'users_id' => \Auth::user()->id,
			'clients_id' => $socialpage->clients_id,
			'iteration' => ($type == 'iteration'),
			'post' => ($type == 'post'),
			'clients_socialpages_id' => $id
		));

		$count = $socialpage->clients()->first()->iterations()->where('iteration_date', '=', date('Y-m-d H:i:s', strtotime($year.'-'.$month.'-'.$day)))->where($type, '=', true)->where('clients_socialpages_id', '=', $socialpage->id)->count();

		$attr = 'target_' . $type . 's';
		$class = SocialPage::getClassByTarget($count, $socialpage->$attr);

		echo $class;
		die;
	}

	public function decrementIteration($id)
	{
		$type = \Input::get('type');
		$day = \Input::get('day');
		$month = \Input::get('month');
		$year = \Input::get('year');

		$socialpage = SocialPage::find($id);
		$date = date('Y-m-d H:i:s', strtotime($year.'-'.$month.'-'.$day));

		$count = \DB::table('iterations')->where('iteration_date', '=', $date)->where('clients_socialpages_id', '=', $id)->count();

		if(!$count)
		{
			echo '';
			die;
		}

		$return = \DB::table('iterations')->where('iteration_date', '=', $date)->where('clients_socialpages_id', '=', $id)->orderBy('id', 'desc')->first();

		$iteration = Iteration::find($return->id);
		$iteration->delete();

		$count = $socialpage->clients()->first()->iterations()->where('iteration_date', '=', date('Y-m-d H:i:s', strtotime($year.'-'.$month.'-'.$day)))->where($type, '=', true)->where('clients_socialpages_id', '=', $socialpage->id)->count();

		$attr = 'target_' . $type . 's';
		$class = SocialPage::getClassByTarget($count, $socialpage->$attr);

		echo $class;
		die;
	}

	public function attachSocialPagesAccounts($clients_id, $socialpages_id)
	{
		$accounts = Account::where('social_network_id', '=', $socialpages_id)->get();

		return view('socialpages_attach_accounts', compact('accounts', 'clients_id', 'socialpages_id'));
	}

	public function postAttachSocialPagesAccounts(Request $request, $clients_id, $socialpages_id)
	{
		$input = \Input::all();

		$validator = \Validator::make($input, array(
			'user' => 'required',
			'password'     => 'required',
			'social_network_id'  => 'required|integer'
		));

		if ($validator->fails())
		{
			$this->throwValidationException(
				$request, $validator
			);
		}

		$social_page = Account::create($input);

		return redirect('admin/clients/'.$clients_id.'/socialpages/'.$socialpages_id.'/attach/accounts');
	}

	public function postDetachSocialPagesAccounts(Request $request, $clients_id, $socialpages_id, $id)
	{
		$account = Account::find($id);

		if(!$account)
		{
			return redirect('admin/clients/'.$clients_id.'/socialpages/'.$socialpages_id.'/attach/accounts')->withErrors(['id' => 'id does not exists.']);
		}

		$account->delete();

		return redirect('admin/clients/'.$clients_id.'/socialpages/'.$socialpages_id.'/attach/accounts');
	}

}
