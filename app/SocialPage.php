<?php namespace SMManager;

use Illuminate\Database\Eloquent\Model;
use SMManager\SocialPage;

class SocialPage extends Model {

	protected $table = 'clients_socialpages';

	protected $fillable = ['social_network', 'address', 'target_posts', 'target_posts_period', 'target_iterations', 'target_iterations_period', 'clients_id'];

	protected $guarded = ['created_at', 'updated_at', 'id'];

	public function clients() {
		return $this->belongsTo('SMManager\\Client', 'clients_id', 'id');
	}

	public function accounts() {
		return $this->hasMany('SMManager\\Account', 'social_network_id', 'id');
	}

	public static function checkTargetInMonth($clients, $dates)
	{
		$targets = array();

		foreach($clients as $client) {
			$social_pages = SocialPage::where('clients_id', '=', $client->id)->get();

			foreach($social_pages as $social_page) {
				for($x=1;$x<=$dates['month_days'];$x++) {
					self::mountTargetMonth($client, $social_page, $dates, $x, $targets);
				}

				$targets[$client->id][$social_page->id] = self::checkTargetPeriod($client, $social_page, $dates, $targets[$client->id][$social_page->id]);
			}
		}

		return $targets;

	}

	public static function mountTargetMonth($client, $social_page, $dates, $day, &$targets)
	{
		$date = date('Y-m-d', strtotime($dates['year'].'-'.$dates['month'].'-'.$day));

		$targets[$client->id][$social_page->id][$date]['count_posts'] = $client->iterations()->where('iteration_date', '=', $date)->where('post', '=', true)->where('clients_socialpages_id', '=', $social_page->id)->count();
		$targets[$client->id][$social_page->id][$date]['target_posts'] = $social_page->target_posts;
		$targets[$client->id][$social_page->id][$date]['target_period_posts'] = $social_page->target_posts_period;

		$targets[$client->id][$social_page->id][$date]['count_iterations'] = $client->iterations()->where('iteration_date', '=', $date)->where('iteration', '=', true)->where('clients_socialpages_id', '=', $social_page->id)->count();
		$targets[$client->id][$social_page->id][$date]['target_iterations'] = $social_page->target_iterations;
		$targets[$client->id][$social_page->id][$date]['target_period_iterations'] = $social_page->target_iterations_period;

	}

	public static function checkTargetPeriod($client, $social_page, $dates, $month_targets)
	{
		// DAYS
		if($social_page->target_posts_period == 'days')
		{
			$month_targets = self::checkTargetPeriodDaily($dates, $month_targets, $social_page->target_posts, 'posts');
		}

		if($social_page->target_iterations_period == 'days')
		{
			$month_targets = self::checkTargetPeriodDaily($dates, $month_targets, $social_page->target_iterations, 'iterations');
		}

		// WEEKS
		if($social_page->target_posts_period == 'weeks')
		{
			$month_targets = self::checkTargetPeriodWeekly($dates, $month_targets, $social_page->target_posts, 'posts');
		}

		if($social_page->target_iterations_period == 'weeks')
		{
			$month_targets = self::checkTargetPeriodWeekly($dates, $month_targets, $social_page->target_iterations, 'iterations');
		}

		return $month_targets;
	}

	public static function checkTargetPeriodDaily($dates, $month_targets, $target, $type)
	{
		foreach($month_targets as &$date_target)
		{
			$dtarget = $date_target['count_' . $type];

			$date_target['class_'.$type] = self::getClassByTarget($dtarget, $target);
		}

		return $month_targets;
	}

	public static function getClassByTarget($dtarget, $target)
	{
		$result = '';

		if($target <= $dtarget)
		{
			$result = 'blue';
		} else if ($dtarget == 0) {
			$result = 'red';
		} else {
			$result = 'gray';
		}

		return $result;
	}

	public static function checkTargetPeriodWeekly($dates, $month_targets, $target, $type)
	{
		$start_date = $target;
	}

}
