<?php namespace SMManager;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

class User extends Model implements AuthenticatableContract, CanResetPasswordContract {

	use Authenticatable, CanResetPassword;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['name', 'email', 'password', 'roles_id'];

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = ['password', 'remember_token'];
    
    protected static $labels = [
        'name' => 'Name', 
        'email' => 'Email', 
        'password' => 'Password', 
        'roles_id' => 'Role',
        'created_at' => 'Created at',
        'updated_at' => 'Updated at'
    ];

    public function roles() {
        return $this->hasMany('SMManager\\Role', 'id', 'roles_id');
    }

    public function iterations() {
        return $this->hasMany('SMManager\\Iteration', 'id', 'users_id');
    }

    public function clients() {
        return $this->belongsToMany('SMManager\\Client', 'users_clients', 'users_id', 'clients_id');
    }
    
    public static function format($instance)
    {
        $data = $instance->toArray();
        
        foreach($data as $key => $value)
        {
            if(in_array($key, ['created_at', 'updated_at']))
            {   
                $data[$key] = self::formatDate($value);
            }

            if(in_array($key, ['roles_id']))
            {
                $data[$key] = $instance->roles()->first()->name;
            }

            $data[self::formatLabel($key)] = $data[$key];
            unset($data[$key]);
        }

        return $data;
    }

    protected static function formatLabel($key)
    {
        if(!in_array($key, array_keys(self::$labels)))
        {
            return $key;
        }

        return self::$labels[$key];        
    }


    public static function formatDate($value)
    {
        return date('d/m/Y H:i:s', strtotime($value));
    }
}
