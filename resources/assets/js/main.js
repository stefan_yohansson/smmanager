
$(document).ready(function() {
    $('.select2').select2({ placeholder : '' });

    $('[data-toggle="tooltip"]').tooltip({
        html: true
    });

    $('.increment-day').on('click', function() {
        var diz = $(this);
        var next = diz.next().next();

        $.ajax({
            type: "GET",
            url: diz.attr('href')
            //data: { name: "John", location: "Boston" }
        })
        .done(function( msg ) {
            next.parent('td').removeClass('red');
            next.parent('td').removeClass('gray');
            next.parent('td').removeClass('blue');
            next.parent('td').addClass(msg);
            next.html(parseInt(next.html()) + 1);
        });

        return false;

    });

    $('.decrement-day').on('click', function() {
        var diz = $(this);
        var next = diz.prev().prev();
        
        $.ajax({
            type: "GET",
            url: diz.attr('href')
            //data: { name: "John", location: "Boston" }
        })
        .done(function( msg ) {
            if(parseInt(next.html()) == 0 && msg == '')
            {
                return false;
            }
            
            next.parent('td').removeClass('red');
            next.parent('td').removeClass('gray');
            next.parent('td').removeClass('blue');
            next.parent('td').addClass(msg);

            next.html(parseInt(next.html()) - 1);
        });


        return false;

    });

    $('.table-clients').stickyTableHeaders();

    $('.hidden-day').on('mouseover', function() {
        $(this).css('opacity', '1');            
    });
    $('.hidden-day').on('mouseout', function() {
        $(this).css('opacity', '0');
    });


});

