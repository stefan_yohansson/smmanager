@extends('app')

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Clients</div>
                <div class="panel-body">
                    <a href="{{ URL::to('admin/clients/create') }}" class="btn btn-success">Add</a>
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Phone</th>
                                <th>Created At</th>
                                <th>Updated At</th>
                                <th>Active</th>
                                <th>Options</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($clients as $client)
                            <tr>
                                <td><a href="{{ URL::to('admin/clients/'.$client->id) }}">{{ $client->name }}</a></td>
                                <td>{{ $client->email }}</td>
                                <td>{{ $client->phone }}</td>
                                <td>{{ SMManager\Client::formatDate($client->created_at) }}</td>
                                <td>{{ SMManager\Client::formatDate($client->updated_at) }}</td>
                                <td><?php echo SMManager\Client::formatBoolean($client->active) ?></td>
                                <td>
                                    <a href="{{ URL::to('admin/clients/'.$client->id.'/edit') }}" class="btn btn-primary">Update</a> 
                                    <a href="{{ URL::to('admin/clients/' . $client->id) }}" class="btn btn-danger" data-method="delete" data-confirm="Are you sure?" data-csrf="{{ csrf_token() }}">Delete</a></td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>

                    <?php echo $clients->render() ?>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
