@extends('app')

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Update a User</div>
                <div class="panel-body">
                    @if (count($errors) > 0)
                        <div class="alert alert-danger">
                            <strong>Whoops!</strong> There were some problems with your input.<br><br>
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif

                    <form class="form-horizontal" role="form" method="POST" action="{{ URL::to('admin/users/' . $id) }}">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <input type="hidden" name="_method" value="PUT">
						<div class="form-group">
							<label class="col-md-4 control-label">Name</label>
							<div class="col-md-6">
								<input type="text" class="form-control" name="name" value="{{ $user->name }}">
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label">E-Mail Address</label>
							<div class="col-md-6">
								<input type="email" class="form-control" name="email" value="{{ $user->email }}">
							</div>
						</div>

                        <div class="form-group">
							<label class="col-md-4 control-label">Role</label>
							<div class="col-md-6">
                                <select class="form-control" name="roles_id">
                                    <option value="1" @if($user->roles_id == 1) selected="selected"  @endif>Admin</option>
                                    <option value="2" @if($user->roles_id == 2) selected="selected" @endif>Social Media</option>
                                </select>
                            </div>
						</div>
                        
                        <div class="form-group">
							<div class="col-md-6 col-md-offset-4">
								<button type="submit" class="btn btn-success">
									Update
								</button>
                                <input type="reset" value="Reset" class="btn btn-warning">
                            </div>
						</div>

                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
