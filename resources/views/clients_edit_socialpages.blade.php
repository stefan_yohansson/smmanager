@extends('app')

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Edit Network to Client</div>
                <div class="panel-body">
                    @if (count($errors) > 0)
                        <div class="alert alert-danger">
                            <strong>Whoops!</strong> There were some problems with your input.<br><br>
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif

                    <form class="form-horizontal" role="form" method="POST" action="{{ URL::to("admin/clients/{$client_id}/edit/socialpages/{$id}") }}">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <input type="hidden" name="clients_id" value="{{ $client_id }}">
						<div class="form-group">
							<label class="col-md-4 control-label">Social Network</label>
                            <div class="col-md-6">
                                <select name="social_network" class="form-control">
                                    <option value="facebook" @if(old('address') == 'facebook' || $social_page->address == 'facebook') selected="selected" @endif>Facebook</option>
                                    <option value="twitter" @if(old('address') == 'twitter' || $social_page->address == 'twitter') selected="selected" @endif>Twitter</option>
                                    <option value="instagram" @if(old('address') == 'instagram' || $social_page->address == 'instagram') selected="selected" @endif>Instagram</option>
                                    <option value="google-plus" @if(old('address') == 'google-plus' || $social_page->address == 'google-plus') selected="selected" @endif>Google+</option>
                                    <option value="linkedin" @if(old('address') == 'linkedin' || $social_page->address == 'linkedin') selected="selected" @endif>LinkedIn</option>
                                    <option value="site" @if(old('address') == 'site' || $social_page->address == 'site') selected="selected" @endif>Site</option>
                                </select>
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label">Address</label>
							<div class="col-md-6">
								<input type="text" class="form-control" name="address" value="{{ $social_page->address }}">
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label">Target Posts</label>
							<div class="col-md-6">
								<input type="number" class="form-control" min="0" name="target_posts" value="{{ $social_page->target_posts }}">
							</div>
						</div>
                        
                        <div class="form-group">
							<label class="col-md-4 control-label">Target Posts Period</label>
							<div class="col-md-6">
                                <select name="target_posts_period" class="form-control">
                                    <option value="days" @if($social_page->target_posts_period == 'days') selected="selected" @endif>Day(s)</option>
                                    <option value="weeks" @if($social_page->target_posts_period == 'weeks') selected="selected" @endif>Week(s)</option>
                                    <option value="months" @if($social_page->target_posts_period == 'months') selected="selected"  @endif>Month(s)</option>
                                    <option value="years" @if($social_page->target_posts_period == 'years') selected="selected" @endif>Year(s)</option>
                                </select>
                            </div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label">Target Iterations</label>
							<div class="col-md-6">
								<input type="number" class="form-control" min="0" name="target_iterations" value="{{ $social_page->target_iterations }}">
							</div>
						</div>
                        
                        <div class="form-group">
							<label class="col-md-4 control-label">Target Iterations Period</label>
							<div class="col-md-6">
                                <select name="target_iterations_period" class="form-control">
                                    <option value="days" @if($social_page->target_iterations_period == 'days') selected="selected" @endif>Day(s)</option>
                                    <option value="weeks" @if($social_page->target_iterations_period == 'weeks') selected="selected" @endif>Week(s)</option>
                                    <option value="months" @if($social_page->target_iterations_period == 'months') selected="selected"  @endif>Month(s)</option>
                                    <option value="years" @if($social_page->target_iterations_period == 'years') selected="selected" @endif>Year(s)</option>
                                </select>
                            </div>
						</div>

                        <div class="form-group">
							<div class="col-md-6 col-md-offset-4">
								<button type="submit" class="btn btn-success">
									Create
								</button>
                                <input type="reset" value="Reset" class="btn btn-warning">
                            </div>
						</div>

                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
