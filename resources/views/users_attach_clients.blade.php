@extends('app')

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Attach Clients</div>
                <div class="panel-body">
                    
                    @if (count($errors) > 0)
                        <div class="alert alert-danger">
                            <strong>Whoops!</strong> There were some problems with your input.<br><br>
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
 
                    <form class="form-horizontal" role="form" method="POST" action="{{ URL::to('admin/users/attach/clients/' . $id) }}">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        
                        @foreach($clients as $client)
                        <div class="form-group">
							<label class="col-md-4 control-label">{{ $client->name }}</label>
							<div class="col-md-6">
                                    <input type="checkbox" class="form-control" @if(in_array($client->id, $attached_clients)) checked="checked" @endif name="clients[]" value="{{ $client->id }}">
                            </div>
						</div>
                        @endforeach
                        
                        <div class="form-group">
							<div class="col-md-6 col-md-offset-4">
								<button type="submit" class="btn btn-success">
									Create
								</button>
                                <input type="reset" value="Reset" class="btn btn-warning">
                            </div>
						</div>

                    </form>

                </div>
            </div>
        </div>
    </div>
</div>

@endsection
