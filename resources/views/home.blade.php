@extends('app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="filter-month col-md-7">
            <form method="GET" action="">
            <div class="form-group inline-block">
                <select id="month" name="month" class="form-control left">
                    @foreach($months_arr as $m => $mv)
                    <option value="{{ $m+1 }}" @if($m+1 == $month) selected @endif>{{ $mv }}</option>
                    @endforeach
                </select>

            </div>
            <div class="form-group inline-block">
                <select id="year" name="year" class="form-control right">
                    @for($x=date('Y')-30;$x<=date('Y');$x++)
                    <option value="{{ $x }}" @if($x == $year) selected @endif>{{ $x }}</option>
                    @endfor
                </select>
            </div>
            <div class="form-group inline-block">
                <select id="clients" name="clients[]" class="form-control right select2" placeholder="Clients" style="min-width: 200px" multiple="multiple">
                    @foreach(SMManager\Client::all() as $client)
                        <option value="{{ $client->id }}" @if(in_array($client->id, $clients_id)) selected @endif>{{ $client->name }}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group inline-block">
                <input type="submit" value="Change" class="btn btn-info form-control right">
            </div>
            </form>
            </div>
            <table class="table-clients table table-bordered table-striped"> 
                <thead>
                    <tr>
                        <th>C/D</th>
                        @for($x=1;$x<=$month_days;$x++)
                        <?php $date = $year.'-'.$month.'-'.$x; ?>
                            <th class="@if(SMManager\Calendar::isDayToday($date)) bg-yellow @endif @if(SMManager\Calendar::isDayWeekend($date)) bg-gray @endif">{{ str_pad($x, 2, "0", STR_PAD_LEFT) }}</th>
                        @endfor
                        <th>Total</th>
                    </tr>
                </thead>

                <tbody>
                    @foreach($clients as $client)
<?php 
$social_pages = SMManager\SocialPage::where('clients_id', '=', $client->id)->get();
$iterator = 0;
 ?>
                        @foreach($social_pages as $social_page)
<?php
    $total_day_post = 0;
    $total_day_iteration = 0;
    $total_social_page = $social_pages->count();
?>
                    @if($social_page->target_posts)
                        <tr class="@if($iterator == 0) first @endif @if($iterator == $total_social_page) last @endif">
                            <td>
                                <div data-toggle="tooltip" data-placement="right" title="@foreach($client->users as $user) {{ $user->name }} <br/> @endforeach @if(!$client->users()->count()) No Users @endif" class="client_name">{{ $client->name }} (P)</div>
                                <br/>
                                <b>{{ $social_page->social_network }}</b>
                                <br/>
                                {{ $social_page->target_posts }} per {{ $social_page->target_posts_period }}
                            </td>
                            @for($x=1;$x<=$month_days;$x++)
<?php
    $date = $year.'-'.$month.'-'.$x;
    $hidden_class = '';
    if(strtotime("now") <= strtotime($date))
    {
        $hidden_class = 'hidden-day';
    }
?>
                                <td class=" @if(SMManager\Calendar::isDayToday($date)) bg-yellow @endif @if(SMManager\Calendar::isDayWeekend($date) && strtotime($date) <= strtotime('now')) bg-gray @endif {{ $hidden_class }} {{ $dates_target[$client->id][$social_page->id][$year.'-'.str_pad($month, 2, 0, STR_PAD_LEFT).'-'.str_pad($x, 2, 0, STR_PAD_LEFT)]['class_posts'] }}">
                                <a href="{{ URL::to('admin/iterations/increment', $social_page->id) . '?' . 'type=post&day=' . $x . '&month=' . $month . '&year=' . $year }}" class="increment-day"><i class="fa fa-angle-double-up"></i></a>
                                <br/>
                                <b>{{ $client->iterations()->where('iteration_date', '=', date('Y-m-d H:i:s', strtotime($year.'-'.$month.'-'.$x)))->where('post', '=', true)->where('clients_socialpages_id', '=', $social_page->id)->count() }}</b>
<?php $total_day_post += $client->iterations()->where('iteration_date', '=', date('Y-m-d H:i:s', strtotime($year.'-'.$month.'-'.$x)))->where('post', '=', true)->where('clients_socialpages_id', '=', $social_page->id)->count() ?>
                                <br/>
                                 <a href="{{ URL::to('admin/iterations/decrement', $social_page->id) . '?' . 'type=post&day=' . $x . '&month=' . $month . '&year=' . $year }}" class="decrement-day"><i class="fa fa-angle-double-down"></i></a> 
                                </td>

                                @if($x == $month_days)
                                    <td>
                                       <b>{{ $total_day_post }}</b>
                                    </td>
                                @endif

                            @endfor
                        </tr>
                    @endif





                    @if($social_page->target_iterations)
                        <tr class="@if($iterator == $total_social_page-1) last @endif">
                            <td>
                                <div data-toggle="tooltip" data-placement="right" title="@foreach($client->users as $user) {{ $user->name }} <br/> @endforeach @if(!$client->users()->count()) No Users @endif" class="client_name">{{ $client->name }} (I)</div>
                                <br/>
                                <b> {{ $social_page->social_network }}</b>
                                <br/>
                                {{ $social_page->target_iterations }} per {{ $social_page->target_iterations_period }}

                            </td>

                            @for($x=1;$x<=$month_days;$x++)
<?php
    $hidden_class = '';

    $date = $year.'-'.$month.'-'.$x;
    if(strtotime("now") <= strtotime($date))
    {
        $hidden_class = 'hidden-day';
    }

?>
                               <td class="@if(SMManager\Calendar::isDayToday($date)) bg-yellow @endif @if(SMManager\Calendar::isDayWeekend($date) && strtotime($date) <= strtotime('now')) bg-gray @endif {{ $hidden_class }} {{ $dates_target[$client->id][$social_page->id][$year.'-'.str_pad($month, 2, 0, STR_PAD_LEFT).'-'.str_pad($x, 2, 0, STR_PAD_LEFT)]['class_iterations'] }}">
                                <a href="{{ URL::to('admin/iterations/increment', $social_page->id) . '?' . 'type=iteration&day=' . $x . '&month=' . $month . '&year=' . $year }}" class="increment-day"><i class="fa fa-angle-double-up"></i></a>
                                <br/>
                                
                               <b>{{ $client->iterations()->where('iteration_date', '=', date('Y-m-d H:i:s', strtotime($year.'-'.$month.'-'.$x)))->where('iteration', '=', true)->where('clients_socialpages_id', '=', $social_page->id)->count() }}</b>
<?php $total_day_iteration += $client->iterations()->where('iteration_date', '=', date('Y-m-d H:i:s', strtotime($year.'-'.$month.'-'.$x)))->where('iteration', '=', true)->where('clients_socialpages_id', '=', $social_page->id)->count() ?>
                                <br/>
                                 <a href="{{ URL::to('admin/iterations/decrement', $social_page->id) . '?' . 'type=iteration&day=' . $x . '&month=' . $month . '&year=' . $year }}" class="decrement-day"><i class="fa fa-angle-double-down"></i></a> 

                                </td>

                                @if($x == $month_days)
                                    <td>
                                        <b>{{ $total_day_iteration }}</b>
                                    </td>
                                @endif

                            @endfor
                        </tr>
                    @endif
                        <?php $iterator++ ?>
                        @endforeach
                    @endforeach 
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection
