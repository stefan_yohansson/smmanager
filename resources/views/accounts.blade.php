@extends('app')

@section('content')
<div class="container-fluid">
  <div class="row">
    <div class="col-md-8 col-md-offset-2">
      <div class="panel panel-default">
        <div class="panel-heading">Accounts</div>
        <div class="panel-body">
            @foreach($users as $user)
              <a href="{{ URL::to('admin/accounts/'.$user->id) }}">{{ $user->name }}</a> &nbsp; | &nbsp;
            @endforeach
        
            @foreach($clients as $client)
              @foreach($client->socialpages as $socialpage)
                @if(count($socialpage->accounts))
                <h2>{{ $client->name }} - {{ $socialpage->social_network }}</h2>
                @foreach($socialpage->accounts as $account)
                  <hr>
                  <p><b>user:</b> {{ $account->user }}</p>
                  <p><b>password:</b> {{ $account->password }}</p>

                @endforeach
                @endif
              @endforeach
            @endforeach
          </div>
        </div>
      </div>
    </div>
  </div>

  @endsection
