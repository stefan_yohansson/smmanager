@extends('app')

@section('content')
<div class="container-fluid">
  <div class="row">
    <div class="col-md-8 col-md-offset-2">
      <div class="panel panel-default">
        <div class="panel-heading">Add Account to Network</div>
        <div class="panel-body">
          @if (count($errors) > 0)
          <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
              @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
              @endforeach
            </ul>
          </div>
          @endif

          <form class="form-horizontal" role="form" method="POST" action="{{ URL::to('admin/clients/'.$clients_id.'/socialpages/' . $socialpages_id . '/attach/accounts' ) }}">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <input type="hidden" name="social_network_id" value="{{ $socialpages_id }}">
            <div class="form-group">
              <label class="col-md-4 control-label">User</label>
              <div class="col-md-6">
                <input type="text" class="form-control" name="user" value="{{ old('user') }}">
              </div>
            </div>

            <div class="form-group">
              <label class="col-md-4 control-label">Password</label>
              <div class="col-md-6">
                <input type="password" class="form-control" name="password" value="{{ old('password') }}">
              </div>
            </div>

            <div class="form-group">
              <div class="col-md-6 col-md-offset-4">
                <button type="submit" class="btn btn-success">
                  Create
                </button>
                <input type="reset" value="Reset" class="btn btn-warning">
              </div>
            </div>

          </form>
        </div>
      </div>

      <table class="table table-striped">
        <thead>
          <th>User</th>
          <th>Password</th>
          <th>Options</th>
        </thead>
        <tbody>
          @foreach ($accounts as $account)
          <tr>
            <td>{{ $account->user }}</td>
            <td>{{ $account->password }}</td>
            <td>
              <a href="{{ URL::to('admin/clients/'.$clients_id.'/socialpages/' . $socialpages_id . '/detach/accounts/' . $account->id ) }}" data-method="delete" data-confirm="Are you sure?" data-csrf="{{ csrf_token() }}" class="btn btn-danger">Delete</a>
            </td>
          </tr>
          @endforeach
        </tbody>
      </table>
    </div>
  </div>
</div>
@endsection
