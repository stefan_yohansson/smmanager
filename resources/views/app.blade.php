<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>LabSocial</title>

	<link href="/css/app.css" rel="stylesheet">
	<link href="{{{ Asset('vendor/select2/select2.css') }}}" rel="stylesheet">
	<link href="{{{ Asset('vendor/select2-bootstrap-css/select2-bootstrap.css') }}}" rel="stylesheet">
	<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
	
	<link rel="icon" type="image/x-icon" href="{{{ Asset('images/favicon.png') }}}">

	<!-- Fonts -->
	<link href='//fonts.googleapis.com/css?family=Roboto:400,300' rel='stylesheet' type='text/css'>

	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
</head>
<body>
	<nav class="navbar navbar-default">
		<div class="container-fluid">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
					<span class="sr-only">Toggle Navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="#"><img src="{{ Asset('images/labsocial_logo_positiva.png') }}" alt="labsocial logo" title="labsocial logo"></a>
			</div>

			<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
				<ul class="nav navbar-nav">
					<li><a href="/">Home</a></li>
					@if(Auth::check() && Auth::user()->roles_id == SMManager\Role::ADMIN)
					<li><a href="{{ URL::to('admin/users') }}">Users</a></li>
					<li><a href="{{ URL::to('admin/roles') }}">Roles</a></li>
					<li><a href="{{ URL::to('admin/clients') }}">Clients</a></li>
					<li><a href="{{ URL::to('admin/accounts') }}">Network Accounts</a></li>
					@endif
				</ul>

				<ul class="nav navbar-nav navbar-right">
					@if (Auth::guest())
					<li><a href="/auth/login">Login</a></li>
					<li><a href="/auth/register">Register</a></li>
					@else
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">{{ Auth::user()->name }} <span class="caret"></span></a>
						<ul class="dropdown-menu" role="menu">
							<li><a href="/auth/logout">Logout</a></li>
						</ul>
					</li>
					@endif
				</ul>
			</div>
		</div>
	</nav>

	@yield('content')
	
	<footer>
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-8 col-md-offset-2">
					<div class="col-md-3">
						<h5>Suporte</h5>
						<ul>
							<li><a href="https://bitbucket.org/stefan_yohansson/smmanager/issues?status=new&status=open" target="_blank">Problemas? Relate-nos</a></li>
						</ul>
					</div>
					<div class="col-md-3">
						<h5>Casos</h5>
						<ul>
							<li><a href="http://agenciaplanob.com.br/" target="_blank">Agência PlanoB</a></li>
							<li><a href="http://stefanyohansson.github.io/" target="_blank">Conheça nossos desenvolvedores</a></li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</footer>

	<!-- Scripts -->
	<script src="{{{ Asset('vendor/jquery/dist/jquery.min.js') }}}"></script>
	<script src="//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.1/js/bootstrap.min.js"></script>
	<script src="{{{ Asset('vendor/StickyTableHeaders/js/jquery.stickytableheaders.min.js') }}}"></script>
	<script src="{{{ Asset('js/laravel.js') }}}"></script>
	<script src="{{{ Asset('vendor/select2/select2.js') }}}"></script>
	<script src="{{{ Asset('js/main.js') }}}"></script>

</body>
</html>
