@extends('app')

@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			<div class="panel panel-default">
				<div class="panel-heading">Role</div>
				<div class="panel-body">
                    <table class="table table-horizontal table-striped">
                        <tbody>
                            @foreach ($role as $key => $value)
                            <tr><td>{{ $key }}</td> <td>{{ $value }}</td></tr>
                            @endforeach
                        </tbody>
                    </table>
                    
                    <div class="col-md-5">
                        <a href="{{ URL::to('admin/roles') }}"  class="btn btn-info">List</a>
                        <a href="{{ URL::to('admin/roles/'.$id.'/edit') }}"  class="btn btn-primary">Update</a>
                        <a href="{{ URL::to('admin/roles/' . $id) }}" class="btn btn-danger" data-method="delete" data-confirm="Are you sure?" data-csrf="{{ csrf_token() }}" >Delete</a>
                    </div>            
                </div>
			</div>
		</div>
	</div>
</div>

@endsection
