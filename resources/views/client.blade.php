@extends('app')

@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			<div class="panel panel-default">
				<div class="panel-heading">Client</div>
				<div class="panel-body">
					<table class="table table-horizontal table-striped">
						<tbody>
							@foreach ($client as $key => $value)
							<tr><td><b>{{ $key }}</b></td> <td><?php echo $value ?></td></tr>
							@endforeach
						</tbody>
					</table>

					<h2>Social Media</h2>
					<table class="table table-striped">
						<thead>
							<th>Name</th>
						</thead>
						<tbody>
							@foreach ($users as $user)
							<tr><td>{{ $user->name }}</td></tr>
							@endforeach
						</tbody>
					</table>

					<h2>Networks</h2>
					<a href="{{ URL::to('admin/clients/attach/socialpages/'.$id) }}" class="btn btn-success">Add</a>
					<table class="table table-striped">
						<thead>
							<th>Network</th>
							<th>Address</th>
							<th>Target Posts</th>
							<th>Target Posts Period</th>
							<th>Target Iterations</th>
							<th>Target Iterations Period</th>
							<th>Options</th>
						</thead>
						<tbody>
							@foreach ($networks as $network)
							<tr>
								<td>{{ $network->social_network }}</td>
								<td>{{ $network->address }}</td>
								<td>{{ $network->target_posts }}</td>
								<td>{{ $network->target_posts_period }}</td>
								<td>{{ $network->target_iterations }}</td>
								<td>{{ $network->target_iterations_period }}</td>
								<td>
									<a href="{{ URL::to('admin/clients/'.$id.'/edit/socialpages/'.$network->id ) }}" class="btn btn-primary">Edit</a>
									<a href="{{ URL::to('admin/clients/'.$id.'/socialpages/'.$network->id . '/attach/accounts/') }}" class="btn btn-warning">Accounts</a>
									<a href="{{ URL::to('admin/clients/detach/socialpages/'.$network->id . '/'.$id) }}" data-method="delete" data-confirm="Are you sure?" data-csrf="{{ csrf_token() }}" class="btn btn-danger">Delete</a>
								</td>
							</tr>
							@endforeach
						</tbody>
					</table>

					<div class="col-md-5">
						<a href="{{ URL::to('admin/clients') }}"  class="btn btn-info">List</a>
						<a href="{{ URL::to('admin/clients/'.$id.'/edit') }}"  class="btn btn-primary">Update</a>
						<a href="{{ URL::to('admin/clients/' . $id) }}" class="btn btn-danger" data-method="delete" data-confirm="Are you sure?" data-csrf="{{ csrf_token() }}" >Delete</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

@endsection
