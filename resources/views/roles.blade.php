@extends('app')

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Roles</div>
                <div class="panel-body">
                    <a href="{{ URL::to('admin/roles/create') }}" class="btn btn-success">Add</a>
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>Name</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($roles as $role)
                            <tr>
                                <td><a href="{{ URL::to('admin/roles/'.$role->id) }}">{{ $role->name }}</a></td>
                                <td>{{ $role->email }}</td>
                                <td><a href="{{ URL::to('admin/roles/'.$role->id.'/edit') }} "class="btn btn-primary">Update</a> <a href="{{ URL::to('admin/roles/' . $role->id) }}" class="btn btn-danger" data-method="delete" data-confirm="Are you sure?" data-csrf="{{ csrf_token() }}">Delete</a></td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>

                    <?php echo $roles->render() ?>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
