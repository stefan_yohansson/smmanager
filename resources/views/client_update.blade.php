@extends('app')

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Update a Client</div>
                <div class="panel-body">
                    @if (count($errors) > 0)
                        <div class="alert alert-danger">
                            <strong>Whoops!</strong> There were some problems with your input.<br><br>
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif

                    <form class="form-horizontal" role="form" method="POST" action="{{ URL::to('admin/clients/' . $id) }}">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <input type="hidden" name="_method" value="PUT">
						<div class="form-group">
							<label class="col-md-4 control-label">Name</label>
							<div class="col-md-6">
								<input type="text" class="form-control" name="name" value="{{ $client->name }}">
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label">E-Mail Address</label>
							<div class="col-md-6">
								<input type="email" class="form-control" name="email" value="{{ $client->email }}">
							</div>
						</div>
                        
                        <div class="form-group">
							<label class="col-md-4 control-label">Phone Contact</label>
							<div class="col-md-6">
								<input type="text" class="form-control" name="phone" value="{{ $client->phone }}">
							</div>
						</div>
                        
                        <div class="form-group">
							<label class="col-md-4 control-label">Active</label>
							<div class="col-md-6">
								<input type="checkbox" class="form-control" name="active" @if($client->active) checked="checked" @endif>
							</div>
						</div>

                        <div class="form-group">
							<div class="col-md-6 col-md-offset-4">
								<button type="submit" class="btn btn-success">
									Update
								</button>
                                <input type="reset" value="Reset" class="btn btn-warning">
                            </div>
						</div>

                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
