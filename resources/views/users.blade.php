@extends('app')

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Users</div>
                <div class="panel-body">
                    <a href="{{ URL::to('admin/users/create') }}" class="btn btn-success">Add</a>
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Created At</th>
                                <th>Updated At</th>
                                <th>Role</th>
                                <th>Options</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($users as $user)
                            <tr>
                                <td><a href="{{ URL::to('admin/users/'.$user->id) }}">{{ $user->name }}</a></td>
                                <td>{{ $user->email }}</td>
                                <td>{{ SMManager\User::formatDate($user->created_at) }}</td>
                                <td>{{ SMManager\User::formatDate($user->updated_at) }}</td>
                                <td>{{ $user->roles()->first()->name }}</td>
                                <td>
<a href="{{ URL::to('admin/users/attach/clients/'. $user->id) }}" class="btn btn-primary">Attach Client</a> 
<a href="{{ URL::to('admin/users/'.$user->id.'/edit') }}" class="btn btn-primary">Update</a> 
<a href="{{ URL::to('admin/users/' . $user->id) }}" class="btn btn-danger" data-method="delete" data-confirm="Are you sure?" data-csrf="{{ csrf_token() }}">Delete</a></td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>

                    <?php echo $users->render() ?>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
